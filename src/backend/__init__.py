# Copyright Mirage authors & contributors <https://github.com/mirukana/mirage>
# and Moment contributors <https://gitlab.com/mx-moment/moment>
# SPDX-License-Identifier: LGPL-3.0-or-later

"""This package provides Moment's backend side that can interact with the UI.

To learn more about how this package works, you might want to check the
documentation in the following modules first:

- `qml_bridge`
- `backend`
- `matrix_client`
- `nio_callbacks`
"""

__app_name__     = "moment"
__display_name__ = "Moment"
__reverse_dns__  = "xyz.mx-moment"
__version__      = "0.7.5"
