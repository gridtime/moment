// Copyright Mirage authors & contributors <https://github.com/mirukana/mirage>
// SPDX-License-Identifier: LGPL-3.0-or-later

import QtQuick 2.12
import QtQuick.Layouts 1.12
import Clipboard 0.1
import "../../.."
import "../../../Base"

HColumnLayout {
    id: historyDelegate

    // Remember timeline goes from newest message at index 0 to oldest
    readonly property var previousModel: historyList.model.get(model.index + 1)
    readonly property var nextModel: historyList.model.get(model.index - 1)
    readonly property QtObject currentModel: model

    readonly property bool isFocused: model.index === historyList.currentIndex

    readonly property bool compact: window.settings.General.compact
    readonly property bool checked: model.id in historyList.checked
    readonly property bool isOwn: true
    readonly property bool isRedacted: false
    readonly property bool onRight: ! historyList.ownEventsOnLeft && isOwn
    readonly property bool combine: false
    readonly property bool talkBreak: false
    readonly property bool dayBreak:
        model.index === 0 ? true : historyList.canDayBreak(previousModel, model)

    readonly property bool hideNameLine: true

    readonly property int cursorShape:
        historyContent.hoveredLink ? Qt.PointingHandCursor :
        historyContent.hoveredSelectable ? Qt.IBeamCursor :
        Qt.ArrowCursor

    readonly property int separationSpacing: theme.spacing * (
        dayBreak  ? 4 :
        talkBreak ? 6 :
        combine && compact ? 0.25 :
        combine ? 0.5 :
        compact ? 1 :
        2
    )

    readonly property alias historyContent: historyContent

    function toggleChecked() {
        historyList.toggleCheck(model.index)
    }

    width: historyList.width - historyList.leftMargin - historyList.rightMargin

    // Needed because of historyList's MouseArea which steals the
    // HSelectableLabel's MouseArea hover events
    onCursorShapeChanged: historyList.cursorShape = cursorShape

    ListView.onRemove: historyList.uncheck(model.id)

    DelegateTransitionFixer {}

    Item {
        Layout.fillWidth: true
        visible: model.index !== 0
        Layout.preferredHeight: separationSpacing
    }

    DayBreak {
        visible: dayBreak

        Layout.fillWidth: true
        Layout.minimumWidth: parent.width
        Layout.bottomMargin: separationSpacing
    }

    HistoryContent {
        id: historyContent

        Layout.fillWidth: true
    }

    TapHandler {
        acceptedButtons: Qt.LeftButton
        acceptedModifiers: Qt.NoModifier
        onTapped: toggleChecked()
    }

    TapHandler {
        acceptedButtons: Qt.LeftButton
        acceptedModifiers: Qt.ShiftModifier
        onTapped: historyList.checkFromLastToHere(model.index)
    }
}
