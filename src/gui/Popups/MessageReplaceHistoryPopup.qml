// Copyright Mirage authors & contributors <https://github.com/mirukana/mirage>
// SPDX-License-Identifier: LGPL-3.0-or-later

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../Base"
import "../Base/Buttons"
import "../Pages/Chat/Timeline"

HColumnPopup {
    id: popup

    contentWidthLimit:
        window.settings.Chat.max_messages_line_length < 0 ?
        theme.controls.popup.defaultWidth * 2 :
        Math.ceil(
            mainUI.fontMetrics.averageCharacterWidth *
            window.settings.Chat.max_messages_line_length
        )
    property var contentHistory

    page.footer: AutoDirectionLayout {
        CancelButton {
            id: cancelButton
            onClicked: popup.close()
        }
    }

    onOpened: cancelButton.forceActiveFocus()

    SummaryLabel {
        text: qsTr("Message History")
        textFormat: Text.StyledText
    }

    HistoryList {
        id: historyList
        historyList.model: contentHistory
        height: 400
        Layout.fillWidth: true
        Layout.fillHeight: true
    }
}
